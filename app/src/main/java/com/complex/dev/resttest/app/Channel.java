
package com.complex.dev.resttest.app;

import java.util.HashMap;
import java.util.Map;

public class Channel {

    private String category;
    private String status;
    private Object subcategory;
    private String language;
    private String title;
    private String channel_url;
    private boolean producer;
    private Object tags;
    private String subcategory_title;
    private int id;
    private String screen_cap_url_large;
    private String meta_game;
    private Object mature;
    private String screen_cap_url_small;
    private String login;
    private String timezone;
    private String screen_cap_url_medium;
    private String screen_cap_url_huge;
    private String category_title;
    private int views_count;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Object subcategory) {
        this.subcategory = subcategory;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel_url() {
        return channel_url;
    }

    public void setChannel_url(String channel_url) {
        this.channel_url = channel_url;
    }

    public boolean isProducer() {
        return producer;
    }

    public void setProducer(boolean producer) {
        this.producer = producer;
    }

    public Object getTags() {
        return tags;
    }

    public void setTags(Object tags) {
        this.tags = tags;
    }

    public String getSubcategory_title() {
        return subcategory_title;
    }

    public void setSubcategory_title(String subcategory_title) {
        this.subcategory_title = subcategory_title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScreen_cap_url_large() {
        return screen_cap_url_large;
    }

    public void setScreen_cap_url_large(String screen_cap_url_large) {
        this.screen_cap_url_large = screen_cap_url_large;
    }

    public String getMeta_game() {
        return meta_game;
    }

    public void setMeta_game(String meta_game) {
        this.meta_game = meta_game;
    }

    public Object getMature() {
        return mature;
    }

    public void setMature(Object mature) {
        this.mature = mature;
    }

    public String getScreen_cap_url_small() {
        return screen_cap_url_small;
    }

    public void setScreen_cap_url_small(String screen_cap_url_small) {
        this.screen_cap_url_small = screen_cap_url_small;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getScreen_cap_url_medium() {
        return screen_cap_url_medium;
    }

    public void setScreen_cap_url_medium(String screen_cap_url_medium) {
        this.screen_cap_url_medium = screen_cap_url_medium;
    }

    public String getScreen_cap_url_huge() {
        return screen_cap_url_huge;
    }

    public void setScreen_cap_url_huge(String screen_cap_url_huge) {
        this.screen_cap_url_huge = screen_cap_url_huge;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
