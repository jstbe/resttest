package com.complex.dev.resttest.app;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private TwitchTvService twitchTvService;

        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://api.justin.tv/api").setLogLevel(RestAdapter.LogLevel.FULL).build();
            twitchTvService = restAdapter.create(TwitchTvService.class);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            final TextView txt1 = (TextView) rootView.findViewById(R.id.texto_1);
            final TextView txt2 = (TextView) rootView.findViewById(R.id.texto_2);
            final TextView txt3 = (TextView) rootView.findViewById(R.id.texto_3);
            final TextView txt4 = (TextView) rootView.findViewById(R.id.texto_4);
            final TextView txt5 = (TextView) rootView.findViewById(R.id.texto_5);
            final TextView txt6 = (TextView) rootView.findViewById(R.id.texto_5);

            Button ok = (Button) rootView.findViewById(R.id.boton_ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    twitchTvService.getStreams(5,0,new Callback<List<POJO>>() {
                        @Override
                        public void success(List<POJO> pojos, Response response) {
                            txt1.setText(pojos.get(0).getTitle());
                            txt2.setText(pojos.get(1).getTitle());
                            txt3.setText(pojos.get(2).getTitle());
                            txt4.setText(pojos.get(3).getTitle());
                            txt5.setText(pojos.get(4).getTitle());
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            txt1.setText("Error");
                        }
                    });
                }
            });

        return rootView;

        }
    }
}
