
package com.complex.dev.resttest.app;

import java.util.HashMap;
import java.util.Map;

public class POJO {

    private int broadcast_part;
    private boolean featured;
    private boolean channel_subscription;
    private String id;
    private String category;
    private String title;
    private int channel_count;
    private int video_height;
    private int site_count;
    private boolean embed_enabled;
    private Channel channel;
    private String up_time;
    private String meta_game;
    private String format;
    private int embed_count;
    private String stream_type;
    private boolean abuse_reported;
    private int video_width;
    private String geo;
    private String name;
    private String language;
    private int stream_count;
    private double video_bitrate;
    private String broadcaster;
    private int channel_view_count;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public int getBroadcast_part() {
        return broadcast_part;
    }

    public void setBroadcast_part(int broadcast_part) {
        this.broadcast_part = broadcast_part;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isChannel_subscription() {
        return channel_subscription;
    }

    public void setChannel_subscription(boolean channel_subscription) {
        this.channel_subscription = channel_subscription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getChannel_count() {
        return channel_count;
    }

    public void setChannel_count(int channel_count) {
        this.channel_count = channel_count;
    }

    public int getVideo_height() {
        return video_height;
    }

    public void setVideo_height(int video_height) {
        this.video_height = video_height;
    }

    public int getSite_count() {
        return site_count;
    }

    public void setSite_count(int site_count) {
        this.site_count = site_count;
    }

    public boolean isEmbed_enabled() {
        return embed_enabled;
    }

    public void setEmbed_enabled(boolean embed_enabled) {
        this.embed_enabled = embed_enabled;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getUp_time() {
        return up_time;
    }

    public void setUp_time(String up_time) {
        this.up_time = up_time;
    }

    public String getMeta_game() {
        return meta_game;
    }

    public void setMeta_game(String meta_game) {
        this.meta_game = meta_game;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public int getEmbed_count() {
        return embed_count;
    }

    public void setEmbed_count(int embed_count) {
        this.embed_count = embed_count;
    }

    public String getStream_type() {
        return stream_type;
    }

    public void setStream_type(String stream_type) {
        this.stream_type = stream_type;
    }

    public boolean isAbuse_reported() {
        return abuse_reported;
    }

    public void setAbuse_reported(boolean abuse_reported) {
        this.abuse_reported = abuse_reported;
    }

    public int getVideo_width() {
        return video_width;
    }

    public void setVideo_width(int video_width) {
        this.video_width = video_width;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getStream_count() {
        return stream_count;
    }

    public void setStream_count(int stream_count) {
        this.stream_count = stream_count;
    }

    public double getVideo_bitrate() {
        return video_bitrate;
    }

    public void setVideo_bitrate(double video_bitrate) {
        this.video_bitrate = video_bitrate;
    }

    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public int getChannel_view_count() {
        return channel_view_count;
    }

    public void setChannel_view_count(int channel_view_count) {
        this.channel_view_count = channel_view_count;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
