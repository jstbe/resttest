package com.complex.dev.resttest.app;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Complex on 05/06/2014.
 */
public interface TwitchTvService {
    @GET("/stream/list.json")
    void getStreams(@Query("limit") int limit, @Query("offset") int offset, Callback<List<POJO>> twcallback);
}

